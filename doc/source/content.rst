********************
Raster Data Plotting
********************

**Raster Data Plotting is a QGIS plugin for creating different plots visualizing raster data.**

The **Raster Data Plotting** plugin adds a panel for creating
i) 2-d scatter/density plots visualizing all visible pixel for two selected raster bands.
The scatter plot data is adapting in real-time, whenever the map canvas extent changes.
ii) spectral profile plots, and
iii) temporal pixel profile plots (requires separate **RasterTimeseriesManager** plugin).

Getting started
===============

Installation:
    In QGIS select **QGIS > Plugins > Manage and Install Plugins...**, search for *RasterDataPlotting* and install the plugin.

Start:
    In the toolbar click |icon| to show the **Raster Data Plotting** panel.

.. |icon| image:: icon.png

Interactive 2D Scatter/Density Plots:
    Select two raster bands (e.g. *Red* and *NIR* bands of a *Landsat 8* image).

    |plot_scatter_gettingstarted1|

    Zoom to full scene extent ...

    |plot_scatter_gettingstarted2|

    ... and then zoom into an urban area.

    |plot_scatter_gettingstarted3|

    Notice how the plot interactively updates with changing map canvas extents.




.. |plot_scatter_gettingstarted1| image:: plot_scatter_gettingstarted1.png

.. |plot_scatter_gettingstarted2| image:: plot_scatter_gettingstarted2.png

.. |plot_scatter_gettingstarted3| image:: plot_scatter_gettingstarted3.png



Interactive plotting:
    Zoom to the raster layer and navigate to specific locations of your choice. Notice how the plot is adopting to the
    map canvas content.

    .. figure:: screenshot.png

        Agricultural area.

    .. figure:: screenshot2.png

        Urban area.

Scatter / Density Plot
======================

The scatter plot is using Cartesian coordinates to display values for two raster bands.
Data density is derived by 2-d histogram binning.
The number of points falling onto the same bin are color-coded.

Spectral region of interests
----------------------------

Create **spectral region of interests** (spectral ROI) inside the scatter plot to overlay/colorize pixels in the map canvas with the same spectral characteristics.

E.g., in this *Landsat 8* image, it is quite easy to identify *Water* and *Forest* pixels inside a *Red vs. NIR* scatter plot:

.. figure:: screenshot3.png

    *Scatter Plot: Landsat 8 Red vs. NIR with two regions capturing Water and Forest pixels.*

.. image:: screenshot5.png
    :width: 49%
.. image:: screenshot4.png
    :width: 49%

*Map Canvas: Landsat 8 in true-color composition (left) and together with spectral ROIs overlay (right).*

Spatial region of interests
---------------------------

Select **spatial region of interests** (spatial ROI)  to focus the analysis on specific regions in the map canvas (e.g. urban, forest, water or agriculture areas).

Given a *Landsat 8* image and some landcover polygons, different kinds of plots are available.

.. image:: spatialroi1.png
    :width: 70%
.. image:: spatialroi1b.png
    :width: 20%

*MapCanvas: Landsat 8 in true- color composition and four different landcover polygons as spatial ROISs.*

.. image:: spatialroi2.png
    :width: 32%
.. image:: spatialroi3.png
    :width: 32%
.. image:: spatialroi4.png
    :width: 32%

*Scatter Plots: density of all pixels (left), density for spatial ROIs (middle), and colorized scatter for spatial ROIs (right).*

Note that for coarser binnings, it is very likely that points from different ROIs will fall into the same bin.
In those cases, the bin color is given by a weighted average of the involved ROI colors.

.. image:: spatialroi5.png
    :width: 100%

Animated plotting
-----------------

Create an animated scatter plots by choosing raster bands of a timeseries that is manages by the `Raster Timeseries Manager <https://raster-timeseries-manager.readthedocs.io/>`_ plugin.

*Example of an animated scatter plot with Tasseled Cap Brightness vs. Greenness.*

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/r9Cxt68Cktg?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Spectral Profile Plot
=====================

.. figure:: spectralprofile.png

    *Spectral Profile Plot: multi-spectral Landsat 8 and hyperspectral HyMap profiles for the same map location.*

Temporal Plot
=============

.. figure:: temporalprofile.png

    *Temporal Profile Plot: Landsat timeseries temporal profiles for
    Tasseled Cap Brightness (white), Greenness (green) and Wetness (blue) for the same map location.*

.. admonition:: Plugin Dependency

    For :guilabel:`Temporal Profile Plots` install the **RasterTimeseriesManager** plugin.

Contact
=======

Please provide feedback to `Andreas Rabe`_ (andreas.rabe\@geo.hu-berlin.de)
or `create an issue`_.

.. _Andreas Rabe: https://www.geographie.hu-berlin.de/de/Members/rabe_andreas
.. _create an issue: https://bitbucket.org/janzandr/rasterdataplotting/issues/new
