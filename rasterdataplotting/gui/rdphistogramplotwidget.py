from collections import OrderedDict
from os.path import join
from typing import List, Optional

import numpy as np
from PyQt5.QtCore import QSize, QRectF, QPointF, Qt, pyqtSignal, QRect, QPoint
from PyQt5.QtGui import QColor, QTransform, QPen, QGuiApplication, QPainter, QImage, QBrush
from PyQt5.QtWidgets import QWidget, QToolButton, QLabel, QSpinBox
from qgis.PyQt import uic
from qgis._core import (QgsRasterLayer, QgsMapLayerProxyModel, QgsSingleBandPseudoColorRenderer,
                        QgsRasterDataProvider, QgsRasterPipe, QgsRasterProjector, QgsRectangle,
                        QgsCoordinateReferenceSystem, QgsRasterBlock, QgsMultiBandColorRenderer, QgsContrastEnhancement,
                        QgsSingleBandGrayRenderer, QgsPalettedRasterRenderer, QgsPointXY, QgsRasterRenderer,
                        QgsCoordinateTransform, QgsProject)
from qgis._gui import QgisInterface, QgsMapCanvas, QgsMapLayerComboBox, QgsColorButton

from rasterdataplotting.rasterdataplotting import rdputils
from rasterdataplotting.rasterdataplotting.site.pyqtgraph.GraphicsScene.mouseEvents import HoverEvent
from rasterdataplotting.rasterdataplotting.site import pyqtgraph as pg
from rasterdataplotting.rasterdataplotting import ui

debug = True


class RdpHistogramPlotWidget(QWidget):
    iface: QgisInterface = None  # set by RdpDockWidget

    mShowPlot: QToolButton
    mLayer: QgsMapLayerComboBox
    mShowOverlay: QToolButton
    mOverlayColor: QgsColorButton
    mOverlayOpacity: QSpinBox
    mCutStretch: QToolButton
    mPlotWidget1: pg.PlotWidget
    mPlotWidget2: pg.PlotWidget
    mPlotWidget3: pg.PlotWidget
    mInfo: QLabel

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        uic.loadUi(join(ui.path, 'histogramplotwidget.ui'), self)
        self.mapCanvas: QgsMapCanvas = self.iface.mapCanvas()
        self.clearData()
        self.clearPlot()

        # init gui
        self.mLayer.setLayer(None)
        self.mLayer.setFilters(QgsMapLayerProxyModel.RasterLayer)
        self.mPlotWidget1.hide()
        self.mPlotWidget2.hide()
        self.mPlotWidget3.hide()

        # connect signals
        self.mLayer.layerChanged.connect(self.onMapCanvasExtentChanged)
        self.mCutStretch.toggled.connect(self.plotData)
        self.mapCanvas.extentsChanged.connect(self.onMapCanvasExtentChanged)
        self.mapCanvas.xyCoordinates.connect(self.onXyCoordinates)
        self.mapCanvas.renderComplete.connect(self.onMapCanvasRenderComplete)
        self.mShowOverlay.toggled.connect(self.mapCanvas.refresh)
        self.mOverlayOpacity.valueChanged.connect(self.mapCanvas.refresh)

        self._overloadEventHandler1()
        self._overloadEventHandler2()
        self._overloadEventHandler3()

    def _overloadEventHandler1(self):

        oldHoverEvent1 = self.mPlotWidget1.plotItem.hoverEvent

        def hoverEvent1(event: HoverEvent):
            oldHoverEvent1(event)
            if event.isExit():
                self.mInfo.setText('')
            else:
                transform: QTransform = self.mPlotWidget1.plotItem.viewTransform()
                x, _ = transform.map(float(event.scenePos().x()), 0)
                binInfo = self.binInfo(keyPlotWidget=self.mPlotWidget1, x=x)
                if binInfo is None:
                    self.mInfo.setText('')
                    return
                barGraphItem = binInfo['bar']
                y = barGraphItem.boundingRect().height()
                yPercentage = round(y / self.mapCanvas.size().width() / self.mapCanvas.size().height() * 100, 2)
                self.mInfo.setText(f'({x}, {yPercentage}%)')
                if event.modifiers() & Qt.ShiftModifier:
                    self.setBinSelection(binInfo=binInfo, select=True)
                if event.modifiers() & Qt.ControlModifier:
                    self.setBinSelection(binInfo=binInfo, select=False)

        self.mPlotWidget1.plotItem.hoverEvent = hoverEvent1

    def _overloadEventHandler2(self):

        oldHoverEvent2 = self.mPlotWidget2.plotItem.hoverEvent

        def hoverEvent2(event: HoverEvent):
            oldHoverEvent2(event)
            if event.isExit():
                self.mInfo.setText('')
            else:
                transform: QTransform = self.mPlotWidget2.plotItem.viewTransform()
                x, _ = transform.map(float(event.scenePos().x()), 0)
                binInfo = self.binInfo(keyPlotWidget=self.mPlotWidget2, x=x)
                if binInfo is None:
                    self.mInfo.setText('')
                    return
                barGraphItem = binInfo['bar']
                y = barGraphItem.boundingRect().height()
                yPercentage = round(y / self.mapCanvas.size().width() / self.mapCanvas.size().height() * 100, 2)
                self.mInfo.setText(f'({x}, {yPercentage}%)')
                if event.modifiers() & Qt.ShiftModifier:
                    self.setBinSelection(binInfo=binInfo, select=True)
                if event.modifiers() & Qt.ControlModifier:
                    self.setBinSelection(binInfo=binInfo, select=False)

        self.mPlotWidget2.plotItem.hoverEvent = hoverEvent2

    def _overloadEventHandler3(self):

        oldHoverEvent3 = self.mPlotWidget3.plotItem.hoverEvent

        def hoverEvent3(event: HoverEvent):
            oldHoverEvent3(event)
            if event.isExit():
                self.mInfo.setText('')
            else:
                transform: QTransform = self.mPlotWidget3.plotItem.viewTransform()
                x, _ = transform.map(float(event.scenePos().x()), 0)
                binInfo = self.binInfo(keyPlotWidget=self.mPlotWidget3, x=x)
                if binInfo is None:
                    self.mInfo.setText('')
                    return
                barGraphItem = binInfo['bar']
                y = barGraphItem.boundingRect().height()
                yPercentage = round(y / self.mapCanvas.size().width() / self.mapCanvas.size().height() * 100, 2)
                self.mInfo.setText(f'({x}, {yPercentage}%)')
                if event.modifiers() & Qt.ShiftModifier:
                    self.setBinSelection(binInfo=binInfo, select=True)
                if event.modifiers() & Qt.ControlModifier:
                    self.setBinSelection(binInfo=binInfo, select=False)

        self.mPlotWidget3.plotItem.hoverEvent = hoverEvent3

    def clearData(self):
        self.arrays = None
        self.maskArrays = None

    def setData(self, arrays, maskArrays):
        for array in arrays:
            assert array is None or isinstance(array, np.ndarray)
        for maskArray in maskArrays:
            assert maskArray is None or isinstance(maskArray, np.ndarray)
        self.arrays = arrays
        self.maskArrays = maskArrays

    def clearPlot(self):
        self.plotInfo = None
        self.mPlotWidget1.clear()
        self.mPlotWidget2.clear()
        self.mPlotWidget3.clear()

    def currentLayer(self) -> QgsRasterLayer:
        layer = self.mLayer.currentLayer()
        return layer

    def currentBinSelections(self):
        if self.plotInfo is None:
            return [None, None, None]

        binSelectionss = list()
        for plotWidget in [self.mPlotWidget1, self.mPlotWidget2, self.mPlotWidget3]:
            binSelections = list()
            for binInfo in self.binInfos(keyPlotWidget=plotWidget):
                barGraphItem: pg.BarGraphItem = binInfo['bar']
                defaultColor: QColor = binInfo['brush_default']
                currentColor: QColor = barGraphItem.opts['brush']
                select = defaultColor != currentColor
                binSelections.append((select, currentColor))
            binSelectionss.append(binSelections)
        return binSelectionss

    def currentBins(self):

        layer = self.currentLayer()
        if layer is None:
            return None
        renderer = layer.renderer()
        n = 128
        if isinstance(renderer, QgsMultiBandColorRenderer):
            redContrastEnhancement: QgsContrastEnhancement = renderer.redContrastEnhancement()
            redBins = np.linspace(
                redContrastEnhancement.minimumValue(), redContrastEnhancement.maximumValue(), n + 1
            )
            redBandName = layer.bandName(renderer.redBand())

            greenContrastEnhancement: QgsContrastEnhancement = renderer.greenContrastEnhancement()
            greenBins = np.linspace(
                greenContrastEnhancement.minimumValue(), greenContrastEnhancement.maximumValue(), n + 1
            )
            greenBandName = layer.bandName(renderer.greenBand())

            blueContrastEnhancement: QgsContrastEnhancement = renderer.blueContrastEnhancement()
            blueBins = np.linspace(
                blueContrastEnhancement.minimumValue(), blueContrastEnhancement.maximumValue(), n + 1
            )
            blueBandName = layer.bandName(renderer.blueBand())

            binss = [redBins, greenBins, blueBins]
            bandNames = [redBandName, greenBandName, blueBandName]
            colorss = [
                [QColor(int(255 * i / n), 0, 0) for i in range(n)],
                [QColor(0, int(255 * i / n), 0) for i in range(n)],
                [QColor(0, 0, int(255 * i / n)) for i in range(n)]
            ]
        elif isinstance(renderer, QgsSingleBandGrayRenderer):
            contrastEnhancement: QgsContrastEnhancement = renderer.contrastEnhancement()
            bins = np.linspace(contrastEnhancement.minimumValue(), contrastEnhancement.maximumValue(), n + 1)
            bandName = layer.bandName(renderer.grayBand())
            binss = [bins, None, None]
            bandNames = [bandName, None, None]
            if renderer.gradient() == 0:
                colorss = [[QColor(*[int(255 * i / (n - 1))] * 3) for i in range(n)], None, None]
            else:
                colorss = [[QColor(*[int(255 - 255 * i / (n - 1))] * 3) for i in range(n)], None, None]
        elif isinstance(renderer, QgsSingleBandPseudoColorRenderer):
            bins = list()
            colors = list()
            for bin in np.linspace(renderer.classificationMin(), renderer.classificationMax(), n + 1):
                color = QColor(*renderer.shader().shade(bin)[1:])
                if len(colors) > 0 and color == colors[-1]:
                    continue  # combine bins with same color (important for the Interpolation=Discrete case)
                bins.append(bin)
                colors.append(color)

            binss = [bins, None, None]
            bandNames = [layer.bandName(renderer.band()), None, None]
            colorss = [colors, None, None]
        elif isinstance(renderer, QgsPalettedRasterRenderer):
            bins = [c.value - 0.5 for c in renderer.classes()]
            bins.append(bins[-1] + 1)
            colors = [c.color.name() for c in renderer.classes()]
            binss = [bins, None, None]
            bandNames = [layer.bandName(renderer.band()), None, None]
            colorss = [colors, None, None]
        else:
            binss = [None] * 3
            bandNames = [None] * 3
            colorss = [None] * 3
        return binss, bandNames, colorss

    def readPointData(self, point: QgsPointXY):
        layer = self.currentLayer()
        if layer is None:
            return
        renderer: QgsRasterRenderer = layer.renderer()
        bands = renderer.usesBands()
        sourceCrs = self.mapCanvas.mapSettings().destinationCrs()
        destCrs = layer.crs()
        coordinateTransform = QgsCoordinateTransform(sourceCrs, destCrs, QgsProject.instance())
        point = coordinateTransform.transform(point)
        provider: QgsRasterDataProvider = layer.dataProvider()
        values = [provider.sample(point, band) for band in bands]
        return values

    def readMapCanvasData(self):
        self.clearData()
        layer = self.currentLayer()
        if layer is None:
            return
        renderer = layer.renderer()
        extent: QgsRectangle = self.mapCanvas.extent()
        size: QSize = self.mapCanvas.size()
        crs: QgsCoordinateReferenceSystem = self.mapCanvas.mapSettings().destinationCrs()

        if isinstance(renderer, (QgsSingleBandPseudoColorRenderer, QgsPalettedRasterRenderer)):
            bands = [renderer.band(), None, None]
        elif isinstance(renderer, QgsSingleBandGrayRenderer):
            bands = [renderer.grayBand(), None, None]
        elif isinstance(renderer, QgsMultiBandColorRenderer):
            bands = [renderer.redBand(), renderer.greenBand(), renderer.blueBand()]
        else:
            bands = [None] * 3

        provider: QgsRasterDataProvider = layer.dataProvider()

        # setup on-the-fly resampling if needed
        if crs != layer.crs():
            pipe = QgsRasterPipe()
            pipe.set(provider.clone())
            projector = QgsRasterProjector()
            projector.setCrs(layer.crs(), crs)
            pipe.insert(2, projector)
        else:
            projector = provider

        # read data
        arrays = list()
        maskArrays = list()
        for band in bands:
            if band is not None:
                block: QgsRasterBlock = projector.block(band, extent, size.width(), size.height())
                array = np.frombuffer(
                    np.array(np.array(block.data())), dtype=rdputils.qgisDataTypeToNumpyDataType(block.dataType())
                )

                # calculate mask from band layer
                mask = np.full_like(array, fill_value=True, dtype=np.bool)
                noDataValues = [obj.min() for obj in provider.userNoDataValues(band)]
                if provider.sourceHasNoDataValue(band) and provider.useSourceNoDataValue(band):
                    noDataValues.append(provider.sourceNoDataValue(band))

                for noDataValue in noDataValues:
                    mask[array == noDataValue] = False

                if array.dtype in [np.float16, np.float32, np.float64]:
                    mask[np.logical_not(np.isfinite(array))] = False
                arrays.append(array)
                maskArrays.append(mask)
            else:
                arrays.append(None)
                maskArrays.append(None)

        self.setData(arrays, maskArrays)

    def plotData(self):
        binSelectionss = self.currentBinSelections()
        self.clearPlot()
        if self.arrays is None:
            return

        binss, bandNames, colorss = self.currentBins()
        plotWidgets = [self.mPlotWidget1, self.mPlotWidget2, self.mPlotWidget3]

        self.plotInfo = OrderedDict()
        for array, maskArray, bins, bandName, colors, binSelections, plotWidget in zip(
                self.arrays, self.maskArrays, binss, bandNames, colorss, binSelectionss, plotWidgets
        ):
            if array is not None:
                data = array[maskArray]
                if not self.mCutStretch.isChecked():
                    vmin = min(np.min(data), bins[0])
                    vmax = max(np.max(data), bins[-1])
                    bins_ = [vmin] + list(bins) + [vmax]
                    colors = [colors[0]] + colors + [colors[-1]]
                else:
                    bins_ = list(bins)

                hist, bin_edges = np.histogram(data, bins=bins_, density=not True)
                for i in range(len(hist)):
                    x = sum(bin_edges[i:i + 2]) / 2
                    width = (bin_edges[i + 1] - bin_edges[i])
                    barGraphItem = pg.BarGraphItem(x=[x], height=hist[i], width=width, brush=colors[i],
                        pen=QColor(0, 0, 0, 0))
                    plotWidget.addItem(barGraphItem)
                    self.setBinInfo(keyPlotWidget=plotWidget, keyI=i, valueBar=barGraphItem, valueBrush=colors[i])
                    if binSelections is not None:
                        select, color = binSelections[i]
                        self.setBinSelection(
                            binInfo=self.binInfo(keyPlotWidget=plotWidget, x=x), select=select, color=color
                        )
                plotWidget.plot(bin_edges, hist, stepMode=True, fillLevel=0, brush=(0, 0, 0, 0))
                rect = QRectF(QPointF(bins[0], 0), QPointF(bins[-1], np.max(hist[1:-1])))
                plotWidget.setRange(rect)
                plotWidget.plotItem.getAxis('bottom').setLabel(text=bandName)
                plotWidget.plotItem.hideAxis('left')
                plotWidget.show()
            else:
                plotWidget.hide()

    def setBinInfo(self, keyPlotWidget: pg.PlotWidget, keyI: int, valueBar: pg.BarGraphItem, valueBrush: QColor):
        self.plotInfo[(keyPlotWidget, keyI)] = {'bar': valueBar, 'brush_default': valueBrush}

    def binInfos(self, keyPlotWidget: pg.PlotWidget):
        return [value for (keyPlotWidget_, keyI), value in self.plotInfo.items() if keyPlotWidget_ is keyPlotWidget]

    def binInfo(self, keyPlotWidget: pg.PlotWidget, x: float):
        for binInfo in self.binInfos(keyPlotWidget=keyPlotWidget):
            rect = binInfo['bar'].boundingRect()
            if rect.x() <= x < rect.x() + rect.width():
                return binInfo
        return None

    def setBinSelection(self, binInfo, select: bool, color: QColor = None):
        if binInfo is None:
            return
        if select:
            if color is None:
                pen = self.mOverlayColor.color()
            else:
                pen = color
            brush = pen
        else:
            pen = QColor(0, 0, 0, 0)
            brush = binInfo['brush_default']

        oldPen = binInfo['bar'].opts['pen']
        binInfo['bar'].setOpts(pen=pen, brush=brush)
        newPen = binInfo['bar'].opts['pen']
        if oldPen != newPen:
            self.mapCanvas.refresh()

    #    def onBinSelectionChanged(self, barGraphItem: pg.BarGraphItem):
    #        self.plotMapCanvasOverlay()

    def onXyCoordinates(self, point):
        selectionModeActive = (
                QGuiApplication.keyboardModifiers() & Qt.ShiftModifier or
                QGuiApplication.keyboardModifiers() & Qt.ControlModifier
        )
        if selectionModeActive:
            data = self.readPointData(point)
            select = QGuiApplication.keyboardModifiers() & Qt.ShiftModifier
            for (x, valid), plotWidget in zip(data, [self.mPlotWidget1, self.mPlotWidget2, self.mPlotWidget3]):
                if valid:
                    binInfo = self.binInfo(keyPlotWidget=plotWidget, x=x)
                    self.setBinSelection(binInfo=binInfo, select=select)

    def onMapCanvasExtentChanged(self):
        if not self.mShowPlot.isChecked():
            return
        self.readMapCanvasData()
        self.plotData()

    def onMapCanvasRenderComplete(self, painter):
        if not self.mShowPlot.isChecked():
            return
        if debug:
            print('onMapCanvasRenderComplete')
        self.overlayData(painter=painter)

    def overlayData(self, painter: QPainter):
        if not self.mShowPlot.isChecked():
            return

        if not self.mShowOverlay.isChecked():
            return

        if debug:
            print('overlayData')

        xsize = self.mapCanvas.size().width()
        ysize = self.mapCanvas.size().height()
        shape = (ysize, xsize)
        r = np.zeros(shape, dtype=np.uint8)
        g = np.zeros(shape, dtype=np.uint8)
        b = np.zeros(shape, dtype=np.uint8)
        a = np.zeros(shape, dtype=np.uint8)

        overlayOpacity = int(255 * self.mOverlayOpacity.value() / 100)
        plotWidgets = [self.mPlotWidget1, self.mPlotWidget2, self.mPlotWidget3]
        for plotWidget, array, maskArray in zip(plotWidgets, self.arrays, self.maskArrays):
            array = np.reshape(array, shape)
            maskArray = np.reshape(maskArray, shape)
            for binInfo in self.binInfos(keyPlotWidget=plotWidget):
                barGraphItem: pg.BarGraphItem = binInfo['bar']
                defaultColor: QColor = binInfo['brush_default']
                currentColor: QColor = barGraphItem.opts['brush']
                if defaultColor != currentColor:
                    xLeft = barGraphItem.boundingRect().x()
                    xRight = barGraphItem.boundingRect().x() + barGraphItem.boundingRect().width()
                    indices = np.logical_and(array >= xLeft, array < xRight)
                    r[indices] = currentColor.red()
                    g[indices] = currentColor.green()
                    b[indices] = currentColor.blue()
                    a[indices] = overlayOpacity

        rgba = np.array([b, g, r, a]).transpose([1, 2, 0])
        rgba = np.require(rgba, np.uint8, 'C')
        qrgba = QImage(rgba.data, rgba.shape[1], rgba.shape[0], rgba.strides[0], QImage.Format_ARGB32)
        painter.drawImage(QRect(QPoint(0, 0), qrgba.size()), qrgba)
