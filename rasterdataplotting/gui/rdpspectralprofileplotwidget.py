from rasterdataplotting.rasterdataplotting.gui.rdpaxiswidget import RdpAxisWidget
from rasterdataplotting.rasterdataplotting.gui.rdpprofileplotwidgetbase import RdpProfilePlotWidgetBase

debug = True


class RdpSpectralProfilePlotWidget(RdpProfilePlotWidgetBase):
    pass

    def axisMode(self):
        return RdpAxisWidget.Mode.SpectralProfile
