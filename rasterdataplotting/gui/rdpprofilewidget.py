from os.path import join

from qgis.PyQt import uic
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtWidgets import *
from qgis.gui import *
from rasterdataplotting.rasterdataplotting.gui.rdpaxiswidget import RdpAxisWidget
from rasterdataplotting.rasterdataplotting import ui

debug = True


class RdpProfileWidget(QWidget):
    sigLocationChanged = pyqtSignal()

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        uic.loadUi(join(ui.path, 'profilewidget.ui'), self)
        self.ui = _Ui(self)
        self._initUi()
        self._connectSignals()

    def _initUi(self):
        pass

    def _connectSignals(self):
        self.ui.axis().sigLocationChanged.connect(lambda: self.sigLocationChanged.emit())


class _Ui(object):

    def __init__(self, obj):
        self.obj = obj

    def show(self):
        assert isinstance(self.obj._show, QToolButton)
        return self.obj._show

    def axis(self):
        assert isinstance(self.obj._axis, RdpAxisWidget)
        return self.obj._axis

    def scale(self):
        assert isinstance(self.obj._scale, QLineEdit)
        return self.obj._scale

    def color(self):
        assert isinstance(self.obj._color, QgsColorButton)
        return self.obj._color
