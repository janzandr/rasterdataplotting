from enmapbox.exampledata import enmap
from qgis.core import *
from enmapbox import EnMAPBox
from enmapbox.testing import initQgisApplication


qgsApp = initQgisApplication()
enmapBox = EnMAPBox(None)
enmapBox.run()

testRaster = True
testTimeseries = not testRaster

#layer = QgsRasterLayer(r'C:\source\QGISPlugIns\enmap-box\enmapboxtestdata\enmap_berlin.bsq', baseName='enmap_berlin')
#roi = None
#layers = [layer]

#QgsProject.instance().addMapLayers(layers)

def test_RdpPlugin():
    from rasterdataplotting.rasterdataplotting.plugin import RdpPlugin

    iface = enmapBox
    iface.addSource(enmap)
    enmapBox.createDock('MAP')
    rdpPlugin = RdpPlugin(iface=iface)
    rdpPlugin.initGui()

    iface.mapCanvas().destinationCrsChanged.emit()
    iface.mapCanvas().layersChanged.emit()

    qgsApp.exec_()

if __name__ == '__main__':
    test_RdpPlugin()
